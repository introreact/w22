import React from "react";

function ComponentName(props) {
    return (
        <div>
            <h1>Hello {props.country}!</h1>
        </div>
    );
}

export default ComponentName;